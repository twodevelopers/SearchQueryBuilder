Search Query Builder
====================

PHP library to create doctrine queries from json format.

Usage
------

### 1) JSON input

```javascript
{
  "query" : [
      {
        "type" : "group",
        "cond" : "or",
        "query" : [
          {"cond" : "or", "type" : "eq", "field" : "q.id", "value" : 1},
          {"cond" : "or", "type" : "eq", "field" : "q.id", "value" : 2}
        ]
      },
      {"cond" : "and", "type" : "eq", "field" : "q.name", "value" : "Test" },
      {"cond" : "and", "type" : "gt", "field" : "q.date", "value" : "10.04.2015" }
  ]
}
```

### 2) Create query builder

```php
// Create event dispatcher
$ed = new EventDispatcher();
$ed->addSubscriber(new EQBuilder());
$ed->addSubscriber(new CompareBuilder());
$ed->addSubscriber(new LikeBuilder());
$ed->addSubscriber(new InBuilder());
$ed->addSubscriber(new MemberOfBuilder());
$ed->addSubscriber(new IntTransformer());
$ed->addSubscriber(new DateTimeTransformer());

// Create query builder
$qb = new QueryBuilder($ed);

// Get doctrine query builder
$doctrineQB = $doctrineEntityManager
                ->getRepository('Test')
                ->createQueryBuilder('q')
                ->orderBy('q.updatedAt', 'desc')
            ;
            
// Define allowed fields and set field options
$allowedFields = [
    'q.id' => [ 'cast_int' => true ], 
    'q.name', 
    'q.createdAt' => [
        'datetime' => [ 'input' => 'd.m.Y', output' => 'Y-m-d H:i:s', 'reset' => false ] 
    ]
];

// Create query
$query = $qb->createQuery($queryData, $allowedFields, $doctrineQB);
```

### 3) How to create a new LineBuilder

Line builder, builds the query. If you need custom functions create your own line builder, 
by extending the [AbstractLineQueryBuilder](https://gitlab.com/twodevelopers/SearchQueryBuilder/blob/master/src/LineBuilder/AbstractLineQueryBuilder.php).
Please have a look at one of the [build in line builder](https://gitlab.com/twodevelopers/SearchQueryBuilder/tree/master/src/LineBuilder) to see a example.

If you have create your own line builder, you need just to attach your line builder to the query builder:

```php

$ed->addSubscriber(new CustomLineBuilder());

```

### 4) How to create a new ValueTransformer

You can use ValueTransformer to transform or validate a incoming value. If you want to create your own value transformer
you need just extends the [AbstractValueTransformer](https://gitlab.com/twodevelopers/SearchQueryBuilder/blob/master/src/ValueTransformer/AbstractValueTransforme).
Please have a look at one of the [build in value transformer](https://gitlab.com/twodevelopers/SearchQueryBuilder/tree/master/src/ValueTransformer) to see a example.

If you have create your own value transformer, you need just to attach your transformer to the query builder:
```php

$ed->addSubscriber(new CustomValueTransformer());

```
