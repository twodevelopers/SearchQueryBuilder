<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 14:11
 */

namespace TwoDevs\SearchQueryBuilder;

class QueryLine
{
    protected $condition;

    protected $field;

    protected $value;

    protected $type;

    protected $extra;

    protected $options;

    protected $query;

    /**
     * @param $condition
     * @param $type
     * @param array $fieldOptions
     */
    public function __construct($condition, $type, array $fieldOptions)
    {
        $this->condition = $condition;
        $this->type = $type;
        $this->options = $fieldOptions;
    }

    /**
     * @param array $query
     * @param array $options
     * @return static
     */
    public static function buildFromArray(array $query, array $options = array())
    {
        if (!isset($query['type'], $query['cond']) && (!isset($query['field']) && 'group' != $query['type'])) {
            throw new \InvalidArgumentException('Cannot create line, missing fields!');
        }

        $q = new static($query['cond'], $query['type'], $options);
        $q->setValue(isset($query['value']) ? $query['value'] : null);
        $q->setField(isset($query['field']) ? $query['field'] : null);

        foreach ($query as $key => $v) {
            if (!in_array($key, ['type', 'cond', 'field', 'value'])) {
                $q->addExtra($key, $v);
            }
        }

        return $q;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param array $extra
     */
    public function setExtra(array $extra)
    {
        $this->extra = $extra;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addExtra($key, $value)
    {
        $this->extra[$key] = $value;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasOption($name)
    {
        return isset($this->options[$name]);
    }

    /**
     * @param $name
     * @return null
     */
    public function getOption($name)
    {
        return $this->hasOption($name) ? $this->options[$name] : null;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
