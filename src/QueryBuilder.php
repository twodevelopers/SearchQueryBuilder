<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 10.03.2015
 * Time: 15:54
 */

namespace TwoDevs\SearchQueryBuilder;

use Doctrine\ORM\QueryBuilder as ORMQueryBuilder;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TwoDevs\SearchQueryBuilder\Event\BeforeLineParseEvent;
use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;
use TwoDevs\SearchQueryBuilder\Event\ValueTransformEvent;
use TwoDevs\SearchQueryBuilder\Exception\QueryBuilderException;
use TwoDevs\SearchQueryBuilder\Helper\Helper;

class QueryBuilder
{
    const ON_VALUE_TRANSFORM = 'query_builder.on_value_transform';
    const ON_LINE_PARSE_BEFORE = 'query_builder.on_line_parse_before';
    const ON_BUILD_LINE_QUERY = 'query_builder.on_build_line_query';

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $query
     * @param array $allowedFields
     * @param ORMQueryBuilder $qb
     * @param null $groupBy
     *
     * @return ORMQueryBuilder
     */
    public function createQuery(array $query, array $allowedFields, ORMQueryBuilder $qb, $groupBy = null)
    {
        if (!isset($query['query'])) {
            throw new QueryBuilderException('Query array is empty');
        }

        $allowedFields = $this->normalizeFieldOptions($allowedFields);

        if (null !== $groupBy) {
            Helper::checkAllowedField($groupBy, $allowedFields);
        }

        $group = $qb->expr()->andX();
        $gAnd = $qb->expr()->andX();
        $gOr = $qb->expr()->orX();

        foreach ($query['query'] as $query) {
            $line = $this->parseLine($query, $qb, $allowedFields);

            if (null !== $line && $line->getQuery()) {
                switch($query['cond']){
                    case 'and':
                        $gAnd->add($line->getQuery());
                        break;
                    case 'or':
                        $gOr->add($line->getQuery());
                        break;
                }
            }
        }

        $qb->andWhere($group->add($gAnd)->add($gOr));

        if (null !== $groupBy) {
            $qb->addGroupBy($groupBy);
            $qb->distinct(true);
        }

        return $qb;
    }

    /**
     * @param array $allowedFields
     * @return array
     */
    protected function normalizeFieldOptions(array $allowedFields)
    {
        $options = array();
        foreach ($allowedFields as $key => $opt) {
            // Das Feld hat keine Optionen
            if (!is_array($opt) && is_string($opt)) {
                $options[$opt] = array();

                // Das Feld hat optionen
            } else {
                $options[$key] = $opt;
            }
        }

        return $options;
    }

    /**
     * @param array $query
     * @param ORMQueryBuilder $qb
     * @param array $allowedFields
     * @param bool $ignoreGroups
     *
     * @return QueryLine|null
     */
    protected function parseLine(array $query, ORMQueryBuilder $qb, array $allowedFields, $ignoreGroups = false)
    {

        // Jede Query muss diese beiden Felder haben
        if (!isset($query['type'], $query['cond']) && (!isset($query['field']) && 'group' != $query['type'])) {
            return null;
        }

        // Wenn es keine Gruppe ist, muss das Feld value vorhanden sein
        if ('group' != $query['type'] && !isset($query['value'])) {
            return null;
        }

        $options = isset($query['field']) && isset($allowedFields[$query['field']]) ? $allowedFields[$query['field']] : array();
        $queryLine = QueryLine::buildFromArray($query, $options);

        // Erlaubt es listeners die Query anzupassen
        $event = new BeforeLineParseEvent($queryLine);
        $this->eventDispatcher->dispatch(self::ON_LINE_PARSE_BEFORE, $event);

        // Throw Exception if field is not allowed
        if ('group' != $queryLine->getType()) {
            Helper::checkAllowedField($queryLine->getField(), $allowedFields);

            // Value Transformer anwenden
            $event = new ValueTransformEvent($queryLine);
            $this->eventDispatcher->dispatch(self::ON_VALUE_TRANSFORM, $event);
        }

        // Wenn ein Listener die Query bereits gebaut hat,
        // können wir hier abbrechen
        $event = new BuildLineQueryEvent($qb, $queryLine);
        if ($this->eventDispatcher->dispatch(self::ON_BUILD_LINE_QUERY, $event)->isPropagationStopped()) {
            return $event->getQueryLine();
        }

        $extras = $queryLine->getExtra();
        if ($queryLine->getType() == 'group' && isset($extras['query']) && !$ignoreGroups) {
            return $this->buildGroup($queryLine, $qb, $allowedFields);
        }

        return null;
    }

    /**
     * @param QueryLine $query
     * @param ORMQueryBuilder $qb
     * @param array $allowedFields
     *
     * @return \Doctrine\ORM\Query\Expr\Base
     */
    protected function buildGroup(QueryLine $query, ORMQueryBuilder $qb, array $allowedFields)
    {
        $exp = ('or' == $query->getCondition()) ? $qb->expr()->orX() : $qb->expr()->andX();

        $orQueries = $qb->expr()->orX();
        $andQueries = $qb->expr()->andX();

        $extras = $query->getExtra();

        foreach ($extras['query'] as $q) {
            if ($q['cond'] == 'or') {
                $line = $this->parseLine($q, $qb, $allowedFields, true);
                if (null !== $line && $line->getQuery()) {
                    $orQueries->add($line->getQuery());
                }
            } else {
                $line = $this->parseLine($q, $qb, $allowedFields, true);
                if (null !== $line && $line->getQuery()) {
                    $andQueries->add($line->getQuery());
                }
            }
        }

        $query->setQuery($exp->add($andQueries)->add($orQueries));
        return $query;
    }

    /**
     * Add a new LineBuilder
     *
     * @param SubscriberInterface $subscriber
     */
    public function addSubscriber(SubscriberInterface $subscriber)
    {
        $this->eventDispatcher->addSubscriber($subscriber);
    }
}
