<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 12:02
 */

namespace TwoDevs\SearchQueryBuilder\ValueTransformer;

use TwoDevs\SearchQueryBuilder\Event\ValueTransformEvent;
use TwoDevs\SearchQueryBuilder\SubscriberInterface;

interface ValueTransformerInterface extends SubscriberInterface
{
    /**
     * @param ValueTransformEvent $event
     *
     * @return void
     */
    public function onValueTransform(ValueTransformEvent $event);
}
