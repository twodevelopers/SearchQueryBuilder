<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 16:44
 */

namespace TwoDevs\SearchQueryBuilder\ValueTransformer;

use TwoDevs\SearchQueryBuilder\Event\ValueTransformEvent;

class IntTransformer extends AbstractValueTransformer
{
    /**
     * @param ValueTransformEvent $event
     *
     * @return void
     */
    public function onValueTransform(ValueTransformEvent $event)
    {
        if (true === $event->getQuery()->getOption('cast_int')) {
            $value = $event->getQuery()->getValue();
            $value = (int) (is_scalar($value) && is_numeric($value)) ? $value : 0;
            $event->getQuery()->setValue($value);
        }
    }
}
