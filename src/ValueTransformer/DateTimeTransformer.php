<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 17:02
 */

namespace TwoDevs\SearchQueryBuilder\ValueTransformer;

use Carbon\Carbon;
use TwoDevs\SearchQueryBuilder\Event\ValueTransformEvent;

class DateTimeTransformer extends AbstractValueTransformer
{
    /**
     * Possible options:
     *      datetime :
     *          input: Y-m-d H:i:s <-- Input format
     *          output: Y-m-d H:i:s <-- Output format
     *          reset: true|false (Should reset value if faild?)
     *
     * @param ValueTransformEvent $event
     *
     * @return void
     */
    public function onValueTransform(ValueTransformEvent $event)
    {
        if (null !== ($options = $event->getQuery()->getOption('datetime'))) {
            $input  = isset($options['input']) ? $options['input'] : 'Y-m-d H:i:s';
            $output = isset($options['output']) ? $options['output'] : 'Y-m-d H:i:s';
            $reset  = (isset($options['reset']) && $options['reset'] === true) ? null : $event->getQuery()->getValue();

            $value = $event->getQuery()->getValue();

            if (null === $value || '' === $value && !$value instanceof \DateTime) {
                $event->getQuery()->setValue($reset);
                return;
            }

            if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
                $event->getQuery()->setValue($reset);
                return;
            }

            $value = trim((string) $value);

            try {
                $dt = Carbon::createFromFormat($input, $value, 'Europe/Berlin');
                if (!$dt || $dt->format($input) != $value) {
                    $event->getQuery()->setValue($reset);
                    return;
                }
            } catch (\Exception $exp) {
                $event->getQuery()->setValue($reset);
                return;
            }

            $event->getQuery()->setValue($dt->format($output));
        }
    }
}
