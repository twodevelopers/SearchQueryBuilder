<?php
/**
 * Created by Zwei Löwen mediawerk GmbH
 * User: Jens Averkamp
 * Date: 13.03.2015
 * Time: 09:50
 */

namespace TwoDevs\SearchQueryBuilder\Exception;

class QueryBuilderException extends \RuntimeException
{
}
