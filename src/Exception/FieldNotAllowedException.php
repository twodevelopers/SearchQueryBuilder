<?php
/**
 * Created by Zwei Löwen mediawerk GmbH
 * User: Jens Averkamp
 * Date: 13.03.2015
 * Time: 10:03
 */

namespace TwoDevs\SearchQueryBuilder\Exception;

class FieldNotAllowedException extends QueryBuilderException
{

}
