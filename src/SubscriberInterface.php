<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 16:13
 */

namespace TwoDevs\SearchQueryBuilder;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

interface SubscriberInterface extends EventSubscriberInterface
{
}
