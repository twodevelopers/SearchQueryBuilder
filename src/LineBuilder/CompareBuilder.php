<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:38
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;

class CompareBuilder extends AbstractLineQueryBuilder
{
    const GT = 'gt';
    const GTE = 'gte';
    const LT = 'lt';
    const LTE = 'lte';

    /**
     * @param BuildLineQueryEvent $event
     * @return void
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event)
    {
        if (!in_array($event->getQueryLine()->getType(), [self::GT, self::GTE, self::LT, self::LTE])) {
            return;
        }

        $qb = $event->getQueryBuilder();
        $line = $event->getQueryLine();

        $qb->setParameter($this->getNextParameterName('value_eq'), $line->getValue());

        if ($line->getType() == self::GT) {
            $line->setQuery($qb->expr()->gt($line->getField(), $this->getCurrentParameterName()));

        } elseif ($line->getType() == self::GTE) {
            $line->setQuery($qb->expr()->gte($line->getField(), $this->getCurrentParameterName()));

        } elseif ($line->getType() == self::LT) {
            $line->setQuery($qb->expr()->lt($line->getField(), $this->getCurrentParameterName()));

        } else {
            $line->setQuery($qb->expr()->lte($line->getField(), $this->getCurrentParameterName()));
        }

        $event->stopBuildQuery();
    }
}
