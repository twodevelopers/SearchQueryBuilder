<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:38
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;

class LikeBuilder extends AbstractLineQueryBuilder
{
    const LIKE = 'like';
    const NOT_LIKE = 'not like';

    /**
     * @param BuildLineQueryEvent $event
     * @return void
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event)
    {

        if (!in_array($event->getQueryLine()->getType(), [self::LIKE, self::NOT_LIKE])) {
            return;
        }

        $qb = $event->getQueryBuilder();
        $line = $event->getQueryLine();

        $qb->setParameter($this->getNextParameterName('value_like'), $line->getValue());

        if ($line->getType() == self::LIKE) {
            $line->setQuery($qb->expr()->like($line->getField(), $this->getCurrentParameterName()));
        } else {
            $line->setQuery($qb->expr()->notLike($line->getField(), $this->getCurrentParameterName()));
        }

        $event->stopBuildQuery();
    }
}
