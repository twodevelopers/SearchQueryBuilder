<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:38
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use Doctrine\ORM\Query\Expr\Comparison;
use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;
use TwoDevs\SearchQueryBuilder\Helper\Helper;

class MemberOfBuilder extends AbstractLineQueryBuilder
{
    const MEMBER_OF = 'member of';
    const NOT_MEMBER_OF = 'not member of';

    /**
     * @param BuildLineQueryEvent $event
     * @return void
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event)
    {

        if (!in_array($event->getQueryLine()->getType(), [self::MEMBER_OF, self::NOT_MEMBER_OF])) {
            return;
        }

        $qb = $event->getQueryBuilder();
        $line = $event->getQueryLine();

        $value = Helper::convertToArrayIfNecessary($line->getValue());
        if ((is_array($value) && count($value) < 1) || (!is_int($value) && !$value)) {
            return;
        }

        $line->setValue($value);
        $qb->setParameter($this->getNextParameterName('value_memberof'), $value);

        if ($line->getType() == self::MEMBER_OF) {
            $line->setQuery(new Comparison($this->getCurrentParameterName(), 'MEMBER OF', $line->getField()));
        } else {
            $line->setQuery(new Comparison($this->getCurrentParameterName(), 'NOT MEMBER OF', $line->getField()));
        }

        $event->stopBuildQuery();
    }
}
