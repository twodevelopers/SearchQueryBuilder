<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 12:17
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\QueryBuilder;

abstract class AbstractLineQueryBuilder implements LineBuilderInterface
{

    private $parameterCounter = 0;
    private $currentParameterName;

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            QueryBuilder::ON_BUILD_LINE_QUERY => 'onBuildLineQuery'
        );
    }

    /**
     * @param string $prefix
     *
     * @return string
     */
    public function getNextParameterName($prefix = 'value')
    {
        $this->parameterCounter++;
        $this->currentParameterName = sprintf(':%s_%d', $prefix, $this->parameterCounter);
        return $this->currentParameterName;
    }

    /**
     * Return the current active parameter name
     *
     * @param string $prefix
     * @return mixed
     */
    public function getCurrentParameterName($prefix = 'value')
    {
        if (!$this->currentParameterName) {
            $this->getNextParameterName($prefix);
        }

        return $this->currentParameterName;
    }
}
