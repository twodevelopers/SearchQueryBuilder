<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:38
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;

class EQBuilder extends AbstractLineQueryBuilder
{
    const EQ = 'eq';
    const NOT_EQ = 'not eq';

    /**
     * @param BuildLineQueryEvent $event
     * @return void
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event)
    {
        if (!in_array($event->getQueryLine()->getType(), [self::EQ, self::NOT_EQ])) {
            return;
        }

        $qb = $event->getQueryBuilder();
        $line = $event->getQueryLine();

        $qb->setParameter($this->getNextParameterName('value_eq'), $line->getValue());

        if ($line->getType() == self::EQ) {
            $line->setQuery($qb->expr()->eq($line->getField(), $this->getCurrentParameterName()));
        } else {
            $line->setQuery($qb->expr()->neq($line->getField(), $this->getCurrentParameterName()));
        }

        $event->stopBuildQuery();
    }
}
