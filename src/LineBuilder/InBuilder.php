<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:38
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;
use TwoDevs\SearchQueryBuilder\Helper\Helper;

class InBuilder extends AbstractLineQueryBuilder
{
    const IN = 'in';
    const NOT_IN = 'not in';

    /**
     * @param BuildLineQueryEvent $event
     * @return void
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event)
    {

        if (!in_array($event->getQueryLine()->getType(), [self::IN, self::NOT_IN])) {
            return;
        }

        $qb = $event->getQueryBuilder();
        $line = $event->getQueryLine();

        $value = Helper::convertToArrayIfNecessary($line->getValue());
        if ((is_array($value) && count($value) < 1) || (!is_int($value) && !$value)) {
            return;
        }

        $line->setValue($value);

        $qb->setParameter($this->getNextParameterName('value_in'), $value);

        if ($line->getType() == self::IN) {
            $line->setQuery($qb->expr()->in($line->getField(), $this->getCurrentParameterName()));
        } else {
            $line->setQuery($qb->expr()->notIn($line->getField(), $this->getCurrentParameterName()));
        }

        $event->stopBuildQuery();
    }
}
