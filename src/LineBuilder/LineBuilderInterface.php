<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:23
 */

namespace TwoDevs\SearchQueryBuilder\LineBuilder;

use TwoDevs\SearchQueryBuilder\SubscriberInterface;
use TwoDevs\SearchQueryBuilder\Event\BuildLineQueryEvent;

interface LineBuilderInterface extends SubscriberInterface
{
    /**
     * @param BuildLineQueryEvent $event
     *
     * @return mixed
     */
    public function onBuildLineQuery(BuildLineQueryEvent $event);

    /**
     * @param string $prefix
     *
     * @return mixed
     */
    public function getNextParameterName($prefix = 'value');

    /**
     * @param string $prefix
     *
     * @return mixed
     */
    public function getCurrentParameterName($prefix = 'value');
}
