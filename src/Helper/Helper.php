<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:26
 */

namespace TwoDevs\SearchQueryBuilder\Helper;

use Carbon\Carbon;
use TwoDevs\SearchQueryBuilder\Exception\FieldNotAllowedException;

class Helper
{
    /**
     * @param $field
     * @param array $allowedFields
     * @param bool $throwException
     *
     * @return bool
     */
    public static function checkAllowedField($field, array $allowedFields, $throwException = true)
    {
        if ($throwException) {
            if (!array_key_exists($field, $allowedFields)) {
                throw new FieldNotAllowedException(
                    sprintf(
                        'Field %s is not allowed! Allowed fields are: %s',
                        $field,
                        implode(', ', array_keys($allowedFields))
                    )
                );
            }

            return true;
        }

        return array_key_exists($field, $allowedFields);
    }

    /**
     * @param $value
     *
     * @return array
     */
    public static function convertToArrayIfNecessary($value)
    {
        if (!is_array($value)) {
            $data = explode(',', $value);
            if (count($data) > 1) {
                return array_map('trim', $data);
            }
        }

        return $value;
    }

    /**
     * Calculate a hash from array
     *
     * @param array $query
     * @return string
     */
    public static function calculateArrayHash(array $query)
    {
        array_multisort($query);
        return md5(json_encode($query));
    }

    /**
     * @param $value
     * @param string $inFormat
     *
     * @return Carbon
     */
    public static function convertDateTime($value, $inFormat = 'Y-m-d H:i:s')
    {
        if (!$value) {
            return Carbon::now('Europe/Berlin');
        }

        if ($value instanceof \DateTime) {
            return $value;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            return Carbon::now('Europe/Berlin');
        }

        $value = trim((string) $value);

        try {
            $dateTime = Carbon::createFromFormat($inFormat, $value, 'Europe/Berlin');
            if (!$dateTime || $dateTime->format($inFormat) != $value) {
                return Carbon::now('Europe/Berlin');
            }
        } catch (\Exception $exp) {
            return Carbon::now('Europe/Berlin');
        }

        return $dateTime;
    }
}
