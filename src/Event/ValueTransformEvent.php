<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 12:19
 */

namespace TwoDevs\SearchQueryBuilder\Event;

use TwoDevs\SearchQueryBuilder\QueryLine;

class ValueTransformEvent extends AbstractQueryBuilderEvent
{
    /** @var QueryLine */
    private $query;

    /**
     * @param QueryLine $query
     */
    public function __construct(QueryLine $query)
    {
        $this->query = $query;
    }

      /**
     * @return QueryLine
     */
    public function getQuery()
    {
        return $this->query;
    }
}
