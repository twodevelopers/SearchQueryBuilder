<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:02
 */

namespace TwoDevs\SearchQueryBuilder\Event;

use Symfony\Component\EventDispatcher\Event;

class AbstractQueryBuilderEvent extends Event
{

}
