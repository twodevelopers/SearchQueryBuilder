<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 12:56
 */

namespace TwoDevs\SearchQueryBuilder\Event;

use TwoDevs\SearchQueryBuilder\QueryLine;

class BeforeLineParseEvent extends AbstractQueryBuilderEvent
{
    /** @var QueryLine */
    private $query;

    /**
     * @param QueryLine $query
     */
    public function __construct(QueryLine $query)
    {
        $this->query = $query;
    }

    /**
     * @return QueryLine
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param array $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * Stops the propagation of the event to further event parser.
     */
    public function stopLineParsing()
    {
        $this->stopPropagation();
    }
}
