<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 24.03.2015
 * Time: 13:15
 */

namespace TwoDevs\SearchQueryBuilder\Event;

use Doctrine\ORM\QueryBuilder as ORMQueryBuilder;
use TwoDevs\SearchQueryBuilder\QueryLine;

class BuildLineQueryEvent extends AbstractQueryBuilderEvent
{
    /** @var ORMQueryBuilder */
    private $queryBuilder;

    /** @var QueryLine */
    private $queryLine;

    /**
     * @param ORMQueryBuilder $queryBuilder
     * @param QueryLine $queryLine
     */
    public function __construct(ORMQueryBuilder $queryBuilder, QueryLine $queryLine)
    {
        $this->queryBuilder = $queryBuilder;
        $this->queryLine = $queryLine;
    }

    /**
     * @return ORMQueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * @return QueryLine
     */
    public function getQueryLine()
    {
        return $this->queryLine;
    }

    /**
     * Stops the propagation of the event to further event parser.
     */
    public function stopBuildQuery()
    {
        $this->stopPropagation();
    }
}
