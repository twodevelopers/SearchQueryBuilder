<?php
/**
 * Created by Two Developers - Sven Motz und Jens Averkamp GbR
 * http://www.two-developers.com
 *
 * User: Jens Averkamp
 * Date: 13.03.2015
 * Time: 10:36
 */

namespace TwoDevs\SearchQueryBuilder\Tests\QueryBuilder;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\EventDispatcher\EventDispatcher;
use TwoDevs\SearchQueryBuilder\LineBuilder\CompareBuilder;
use TwoDevs\SearchQueryBuilder\LineBuilder\EQBuilder;
use TwoDevs\SearchQueryBuilder\LineBuilder\InBuilder;
use TwoDevs\SearchQueryBuilder\LineBuilder\LikeBuilder;
use TwoDevs\SearchQueryBuilder\LineBuilder\MemberOfBuilder;
use TwoDevs\SearchQueryBuilder\QueryBuilder;
use TwoDevs\SearchQueryBuilder\ValueTransformer\DateTimeTransformer;
use TwoDevs\SearchQueryBuilder\ValueTransformer\IntTransformer;

class QueryBuilderTest extends \PHPUnit_Framework_TestCase
{
    protected $em;

    protected $qb;

    protected $ed;

    protected function setUp()
    {
        $config = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/Fixtures/Entity'], true);
        $dbParams = [
            'driver' => 'pdo_sqlite',
            'memory' => true,
            'charset' => 'UTF8'
        ];

        $this->em = EntityManager::create($dbParams, $config);
        $this->qb = $this->em
            ->getRepository('TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity')
            ->createQueryBuilder('q');

        $this->ed = new EventDispatcher();
        $this->ed->addSubscriber(new EQBuilder());
        $this->ed->addSubscriber(new CompareBuilder());
        $this->ed->addSubscriber(new LikeBuilder());
        $this->ed->addSubscriber(new InBuilder());
        $this->ed->addSubscriber(new MemberOfBuilder());
        $this->ed->addSubscriber(new IntTransformer());
        $this->ed->addSubscriber(new DateTimeTransformer());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        if ($this->em) {
            $this->em->close();
        }

    }

    public function testEqBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/eq.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.id = :value_eq_1 AND q.id <> :value_eq_2',
            $query->getDQL()
        );
    }

    public function testCompareBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/compare.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.id > :value_eq_1 AND q.id >= :value_eq_2 AND q.id < :value_eq_3 AND q.id <= :value_eq_4',
            $query->getDQL()
        );
    }

    public function testLikeBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/like.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.id LIKE :value_like_1 AND q.id NOT LIKE :value_like_2',
            $query->getDQL()
        );
    }

    public function testInBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/in.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.id IN(:value_in_1) AND q.id NOT IN(:value_in_2)',
            $query->getDQL()
        );
    }

    public function testMemberOfBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/memberOf.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE :value_memberof_1 MEMBER OF q.id AND :value_memberof_2 NOT MEMBER OF q.id',
            $query->getDQL()
        );
    }

    public function testGroupBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/group.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.id', 'q.name'], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.name = :value_eq_3 AND (q.id = :value_eq_1 OR q.id = :value_eq_2)',
            $query->getDQL()
        );
    }

    /**
     * @expectedException \TwoDevs\SearchQueryBuilder\Exception\FieldNotAllowedException
     */
    public function testNotAllowedFieldsBuildQuery()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/eq.json'), true);
        $qb = new QueryBuilder($this->ed);
        $qb->createQuery($queryData, ['q.name'], $this->qb);
    }

    public function testWithExternalWheres()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/eq.json'), true);

        $qb = new QueryBuilder($this->ed);

        $rqb = $this->qb
                ->andWhere('q.available = true')
        ;

        $query = $qb->createQuery($queryData, ['q.id', 'q.name'], $rqb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.available = true AND (q.id = :value_eq_1 AND q.id <> :value_eq_2)',
            $query->getDQL()
        );
    }

    public function testWithExternalWheresAndGroups()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/group.json'), true);

        $qb = new QueryBuilder($this->ed);

        $rqb = $this->qb
            ->andWhere('q.available = true')
        ;

        $query = $qb->createQuery($queryData, ['q.id', 'q.name'], $rqb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.available = true AND (q.name = :value_eq_3 AND (q.id = :value_eq_1 OR q.id = :value_eq_2))',
            $query->getDQL()
        );
    }

    public function testDateTimeTransformer()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/datetime_transformer.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.datetime' => ['datetime' => ['input' => 'd.m.Y H:i:s', 'output' => 'Y-m-d H:i:s', 'reset' => true]]], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.datetime = :value_eq_1 AND q.datetime = :value_eq_2',
            $query->getDQL()
        );

        $this->assertEquals('2015-03-24 18:00:01', $query->getParameter('value_eq_1')->getValue());
        $this->assertEquals(null, $query->getParameter('value_eq_2')->getValue());


        $this->qb = $this->em->getRepository('TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity')->createQueryBuilder('q');
        $query = $qb->createQuery($queryData, ['q.datetime' => ['datetime' => ['input' => 'd.m.Y H:i:s', 'output' => 'Y-m-d', 'reset' => true]]], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.datetime = :value_eq_3 AND q.datetime = :value_eq_4',
            $query->getDQL()
        );

        $this->assertEquals('2015-03-24', $query->getParameter('value_eq_3')->getValue());
        $this->assertEquals(null, $query->getParameter('value_eq_4')->getValue());

    }

    public function testIntTransformer()
    {
        $queryData = json_decode(file_get_contents(__DIR__ . '/Fixtures/int_transformer.json'), true);

        $qb = new QueryBuilder($this->ed);
        $query = $qb->createQuery($queryData, ['q.number' => ['cast_int' => true]], $this->qb);

        $this->assertEquals(
            'SELECT q FROM TwoDevs\SearchQueryBuilder\Tests\Fixtures\Entity\TestEntity q WHERE q.number = :value_eq_1 AND q.number = :value_eq_2 AND q.number = :value_eq_3',
            $query->getDQL()
        );

        $this->assertEquals(99, $query->getParameter('value_eq_1')->getValue());
        $this->assertEquals(0, $query->getParameter('value_eq_2')->getValue());
        $this->assertEquals(0, $query->getParameter('value_eq_3')->getValue());
    }
}
